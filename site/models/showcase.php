<?php

/**
* @package     Joomla.Site
* @subpackage  com_atoms
*
* @copyright   Copyright (C) Atom-S LLC. All rights reserved.
* @license     GNU General Public License version 3 or later; see LICENSE.txt
*/

// No direct access
defined( '_JEXEC' ) or die;

/**
 * @author Korotkov Vadim
 * 
 * @since  1.0.0
 */
class AtomsModelShowcase extends JModelLegacy
{
    /**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @since   1.0.0
	 *
	 * @return void
	 */
	protected function populateState()
	{
		$app = JFactory::getApplication('site');
		// Load the parameters.
		$params = $app->getParams();
		$this->setState('params', $params);
	} 
    
    /**
	 * The method of obtaining tour windows.
	 *	 
	 * @return  object
	 *
	 * @since   1.0.0
	 */
	public function getItems()
	{
        $app    = JFactory::getApplication();
        $items  = new stdClass();
        // get params
        $sef = $app->getCfg('sef', '1');
        $ssl = $app->getCfg('force_ssl', '0');
        $params = $this->getState('params');
        
        $apiUrl = $params->get('host_url', '');
        $apiKey = $params->get('api_key', '');
        $apiVersion = $params->get('api_version', '');

        try
        {
            
            if( $sef == '1' ) 
            {
                
                $rewrite = $app->getCfg('sef_rewrite', '0');
                
                $originalLink = JRoute::_($app->getMenu()->getActive()->link, false, (($ssl=='2')?true:-1) );
                
                $uri = JUri::getInstance();
                $currentLink = $uri->toString(array('scheme', 'host', 'path', 'query'));
                
                if( $rewrite == '1' && ($originalLink != $currentLink && strstr($currentLink, 'index.php')) )
                {
                    return $app->redirect($originalLink);
                }
                elseif( $originalLink != $currentLink && str_replace(array('index.php/', 'index.php'), '', $originalLink) != $currentLink )
                {
                    return JError::raiseError(404, JText::_('COM_ATOMS_ERROR_SHOWCASE_NOT_FOUND'));
                }
                
            }
            
            // get showcase api url
            $apiShowcaseUrl = AtomsSiteHelper::$apiShowcaseUrl;
            // buil link api tours
            $apiShowcaseUrl = AtomsSiteHelper::buildLink( $apiShowcaseUrl, array('{api_version}', '{api_key}'), array($apiVersion, $apiKey), $apiUrl );
            // connect and get json data of atom-s
            @$items = json_decode(file_get_contents($apiShowcaseUrl, false, stream_context_create(AtomsSiteHelper::$verifyOptionsSSL)));
                    
            if( empty((array) $items) ) {
                $this->setError(JText::_('COM_ATOMS_ERROR_TOURS_NOT_FOUND'));
            }
        }
        catch( Exception $e )
        {
            return JError::raiseError(404, $e->getMessage());
        }

		return $items;
	} 
    
}